package com.seda.codeeval.hard.wordsearch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	private static final char[][] GRID = { { 'A', 'B', 'C', 'E' }, { 'S', 'F', 'C', 'S' }, { 'A', 'D', 'E', 'E' } };

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = br.readLine()) != null) {
			if (hasMatchWord(GRID, line.trim().toCharArray())) {
				System.out.println("True");
			} else {
				System.out.println("False");
			}
		}
	}

	private static boolean hasMatchWord(char[][] grid, char[] word) {
		boolean[][] used = new boolean[3][4];

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 4; col++) {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 4; j++) {
						used[i][j] = false;
					}
				}
				if (hasMatchWord(grid, used, word, 0, row, col)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean hasMatchWord(char[][] grid, boolean[][] used, char[] word, int index, int row, int col) {
		if (row < 0 || row == 3 || col < 0 || col == 4) {
			return false;
		}
		if (used[row][col]) {
			return false;
		}
		if (grid[row][col] != word[index]) {
			return false;
		}
		used[row][col] = true;
		if (index + 1 == word.length) {
			return true;
		}
		return hasMatchWord(grid, used, word, index + 1, row + 1, col)
				|| hasMatchWord(grid, used, word, index + 1, row - 1, col)
				|| hasMatchWord(grid, used, word, index + 1, row, col - 1)
				|| hasMatchWord(grid, used, word, index + 1, row, col + 1);
	}
}