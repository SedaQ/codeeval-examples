package com.seda.codeeval.hard.stringpermutations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			Set<String> res = permutation(line.trim());
			Iterator<String> it = res.iterator();
			while (it.hasNext()) {
				System.out.print(it.next());
				if (it.hasNext()) {
					System.out.print(",");
				}
			}
			System.out.println();
		}
		br.close();
	}

	public static Set<String> permutation(String s) {
		char[] array = s.toCharArray();
		Set<String> res = new TreeSet<String>();
		permutation(array, 0, res);
		return res;
	}

	private static void permutation(char[] arr, int index, Set<String> res) {
		if (index == arr.length - 1) {
			res.add(new String(arr));
			return;
		}
		for (int i = index; i < arr.length; i++) {
			swap(arr, i, index);
			permutation(arr, index + 1, res);
			swap(arr, i, index);
		}
	}

	private static void swap(char[] arr, int ind1, int ind2) {
		char temp = arr[ind1];
		arr[ind1] = arr[ind2];
		arr[ind2] = temp;
	}
}
