package com.seda.codeeval.hard.repeatedsubstring;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";

		while ((line = br.readLine()) != null) {
			if (line.length() > 0) {
				String result = longestRepSubstr(line.trim());
				if (result != null) {
					System.out.println(result);
				} else {
					System.out.println("NONE");
				}
			}
		}
		br.close();
	}

	public static String longestRepSubstr(String str) {
		return longestRepSubstr(str.toCharArray());
	}

	private static String longestRepSubstr(char[] str) {
		boolean fndRepSubstr = false;
		int bestIndex = 0;
		int bestLength = 0;
		for (int width = 1; width <= str.length / 2; width++) {
			for (int offset = 0; offset <= str.length - (2 * width); offset++) {
				String base = new String(str, offset, width);
				if (isStringAllWhitespace(base)) {
					continue;
				}
				boolean continueToNextOffset = true;
				for (int current = offset + width; current <= str.length - width; current++) {
					String compare = new String(str, current, width);
					if (base.equals(compare)) {
						fndRepSubstr = true;
						bestIndex = offset;
						bestLength = width;

						continueToNextOffset = false;
						break;
					}
				}
				if (!continueToNextOffset) {
					break;
				}
			}
		}
		if (fndRepSubstr) {
			return new String(str, bestIndex, bestLength);
		} else {
			return null;
		}
	}

	private static boolean isStringAllWhitespace(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isWhitespace(c)) {
				return false;
			}
		}
		return true;
	}
}
