package com.seda.codeeval.hard.climbingstairs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

	private static Map<Integer, Long> map = new HashMap<>();

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		map.put(1, 1L);
		map.put(2, 2L);
		while ((line = br.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			String value = String.valueOf(numberOfStairs(Integer.parseInt(line.trim())));
			System.out.println(value);
		}
		br.close();
	}

	public static long numberOfStairs(int number) {
		if (map.get(number) != null) {
			return map.get(number).longValue();
		}
		long result = numberOfStairs(number - 1) + numberOfStairs(number - 2);
		map.put(number, result);
		return result;
	}
}