import sys

ways = {1: 1, 2: 2}
def cWaysToClimb(strs):
    if strs in ways:
        return ways[strs]
    res = cWaysToClimb(strs-1) + cWaysToClimb(strs-2)
    ways[strs] = res
    return res

test_cases = open(sys.argv[1], 'r')
for test in test_cases:
    test = test.strip()
    if len(test) == 0:
        continue
    print(cWaysToClimb(int(test)))

test_cases.close()
