package com.seda.codeeval.hard.telephonewords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader bf = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = bf.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			String test = line.trim();
			// System.out.println(Collectors.joining(",",));
		}
	}

	public static String getTelephoneWords(char[] number, String prefix, int index) {
		if (index == number.length) {
			return prefix;
		}
		Map<Character, String> map = new HashMap<>();
		map.put('0', "0");
		map.put('1', "1");
		map.put('2', "abc");
		map.put('3', "def");
		map.put('4', "ghi");
		map.put('5', "jkl");
		map.put('6', "mno");
		map.put('7', "pqrs");
		map.put('8', "tuv");
		map.put('9', "wxyz");
		String result = "";

		for (Map.Entry<Character, String> entry : map.entrySet()) {
			result += getTelephoneWords(number, prefix + entry.getValue(), index + 1);
		}
		return result;
	}
}
