package com.seda.codeeval.hard.closestpair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader in = new BufferedReader(new FileReader(f));
		Set<Point> set = new HashSet<>();
		while (true) {
			set = parseSet(in);
			if (set.size() == 0) {
				break;
			}
			double dist = minDistanceInSet(set);
			if (dist == Double.POSITIVE_INFINITY) {
				System.out.println("INFINITY");
			} else {
				System.out.println(String.format("%.4f", dist));
			}
		}
	}

	public static Set<Point> parseSet(BufferedReader br) throws IOException {
		int n = Integer.parseInt(br.readLine());
		HashSet<Point> set = new HashSet<Point>();
		for (int i = 0; i < n; i++) {
			String[] linArr = br.readLine().split(" ");
			Point p = new Point(Integer.parseInt(linArr[0]), Integer.parseInt(linArr[1]));
			set.add(p);
		}
		return set;
	}

	public static double minDistanceInSet(Set<Point> set) {
		double minDist = Double.POSITIVE_INFINITY;
		for (Point comparator : set) {
			for (Point p : set) {
				if (comparator.equals(p)) {
					continue;
				}
				double dist = comparator.distanceTo(p);
				if (dist < 10000.0 && dist < minDist) {
					minDist = dist;
				}
			}
		}

		return minDist;
	}

	public static class Point {
		private final int _x;
		private final int _y;

		public Point(int x, int y) {
			_x = x;
			_y = y;
		}

		public double distanceTo(Point p) {
			int x = (_x - p._x) * (_x - p._x);
			int y = (_y - p._y) * (_y - p._y);

			return Math.sqrt(x + y);
		}

		public boolean equals(Point p) {
			return _x == p._x && _y == p._y;
		}
	}
}
