package com.seda.codeeval.hard.textdolar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			System.out.println(numberToText(Integer.parseInt(line.trim())).trim() + "Dollars");
		}
		br.close();
	}

	public static String numberToText(int n) {
		String result = "";
		if (n < 20) {
			switch (n) {
			case 1:
				return "One";
			case 2:
				return "Two";
			case 3:
				return "Three";
			case 4:
				return "Four";
			case 5:
				return "Five";
			case 6:
				return "Six";
			case 7:
				return "Seven";
			case 8:
				return "Eight";
			case 9:
				return "Nine";
			case 10:
				return "Ten";
			case 11:
				return "Eleven";
			case 12:
				return "Twelve";
			case 13:
				return "Thirteen";
			case 14:
				return "Fourteen";
			case 15:
				return "Fifteen";
			case 16:
				return "Sixteen";
			case 17:
				return "Seventeen";
			case 18:
				return "Eighteen";
			case 19:
				return "Nineteen";
			}
		} else if (n < 100) {
			switch ((int) Math.floor(n / 10.0) * 10) {
			case 20:
				result += "Twenty";
				break;
			case 30:
				result += "Thirty";
				break;
			case 40:
				result += "Forty";
				break;
			case 50:
				result += "Fifty";
				break;
			case 60:
				result += "Sixty";
				break;
			case 70:
				result += "Seventy";
				break;
			case 80:
				result += "Eighty";
				break;
			case 90:
				result += "Ninety";
				break;
			}
			if (n % 10 > 0) result += numberToText(n % 10);
			return result;
		} else if (n < 1000) {
			result = numberToText(n / 100) + "Hundred";
			if (n % 100 > 0) result += numberToText(n % 100);
			return result;
		} else if (n < 1_000_000) {
			result = numberToText(n / 1000) + "Thousand";
			if (n % 1000 > 0) result += numberToText(n % 1000);
			return result;
		} else if (n < 1_000_000_000) {
			result = numberToText(n / 1_000_000) + "Milion";
			if (n % 1_000_000 > 0) result += numberToText(n % 1_000_000);
			return result;
		} else {
			result = numberToText(n / 1_000_000_000) + "Bilion";
			if (n % 1_000_000_000 > 0) result += numberToText(n % 1_000_000_000);
			return result;
		}
		return result;
	}
}