package com.seda.codeeval.easy.uniqueelements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainWithoutIntArr {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			String[] numbers = line.trim().split(",");
			StringBuilder sb = new StringBuilder().append(numbers[0]).append(",");
			for (int i = 1; i < numbers.length; i++) {
				if (i > 0 && numbers[i].equals(numbers[i - 1])) {
					continue;
				} else {
					sb.append(numbers[i]).append(",");
				}
			}
			if (sb.charAt(sb.length() - 1) == ',') {
				sb.deleteCharAt(sb.length() - 1);
			}
			System.out.println(sb.toString());
		}
		buffer.close();
	}
}