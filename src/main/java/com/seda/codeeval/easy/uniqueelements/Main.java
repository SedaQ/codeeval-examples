package com.seda.codeeval.easy.uniqueelements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			String[] numbers = line.trim().split(",");
			int[] intArray = new int[numbers.length];
			for (int i = 0; i < numbers.length; i++) {
				intArray[i] = Integer.parseInt(numbers[i]);
			}
			StringBuilder sb = new StringBuilder().append(intArray[0]).append(",");
			for (int i = 1; i < intArray.length; i++) {
				if (i > 0 && intArray[i] == intArray[i - 1]) {
					continue;
				} else {
					sb.append(intArray[i]).append(",");
				}
			}
			if (sb.charAt(sb.length() - 1) == ',') {
				sb.deleteCharAt(sb.length() - 1);
			}
			System.out.println(sb.toString());
		}
		buffer.close();
	}
}
