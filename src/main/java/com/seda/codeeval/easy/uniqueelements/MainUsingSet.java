package com.seda.codeeval.easy.uniqueelements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MainUsingSet {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			Set<String> array = new TreeSet<>(Arrays.stream(line.trim().split(",")).collect(Collectors.toSet()));
			System.out.println(array.stream().collect(Collectors.joining(",")));
		}
		buffer.close();
	}
}
