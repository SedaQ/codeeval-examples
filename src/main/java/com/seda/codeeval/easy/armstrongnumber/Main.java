package com.seda.codeeval.easy.armstrongnumber;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		int sum = 0;
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			char[] c = line.toCharArray();
			for (int i = 0; i < c.length; i++) {
				sum += Math.pow(Integer.parseInt(String.valueOf(c[i])), line.length());
			}
			if (sum == Integer.parseInt(line)) {
				System.out.println("True");
			} else {
				System.out.println("False");
			}
			sum = 0;
		}
		br.close();
	}
}