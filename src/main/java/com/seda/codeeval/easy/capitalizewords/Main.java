package com.seda.codeeval.easy.capitalizewords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			char[] c = line.trim().toCharArray();
			StringBuilder sb = new StringBuilder();
			c[0] = Character.toUpperCase(c[0]);
			sb.append(c[0]);
			for (int i = 0; i < c.length; i++) {
				if (i > 0) {
					if (c[i - 1] == ' ') {
						c[i] = Character.toUpperCase(c[i]);
					}
					sb.append(c[i]);
				}
			}
			System.out.println(sb.toString());
		}
		buffer.close();
	}
}
