package com.seda.codeeval.easy.mersenneprime;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = br.readLine()) != null) {
			int number = Integer.parseInt(line.trim());
			StringBuilder sb = new StringBuilder();
			int temp = 3;
			int i = 3;
			while (temp < number) {
				if (isPrime(temp)) {
					sb.append(temp).append(", ");
				}
				temp = (int) Math.pow(2, i) - 1;
				i++;
			}
			System.out.println(sb.deleteCharAt(sb.length() - 1).deleteCharAt(sb.length() - 1).toString());
		}
		br.close();
	}

	public static boolean isPrime(int n) {
		int root = (int) Math.sqrt(n);
		for (int i = 2; i <= root; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
