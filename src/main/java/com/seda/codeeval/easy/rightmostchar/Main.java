package com.seda.codeeval.easy.rightmostchar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			String[] lineArr = line.split(",");
			char posOfChar = lineArr[1].trim().charAt(0);
			char[] c = lineArr[0].trim().toCharArray();
			int lastPosition = 0;
			boolean found = false;
			for (int i = 0; i < c.length; i++) {
				if (c[i] == posOfChar) {
					lastPosition = i;
					found = true;
				}
			}
			if (found == true) {
				System.out.println(lastPosition);
			} else {
				System.out.println(-1);
			}
		}
		buffer.close();
	}
}
