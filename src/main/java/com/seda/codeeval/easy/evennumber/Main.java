package com.seda.codeeval.easy.evennumber;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			System.out.println(isEven(Integer.parseInt(line.trim())) ? "1" : "0");
		}
		br.close();
	}

	public static boolean isEven(int number) {
		return number % 2 == 0;
	}
}
