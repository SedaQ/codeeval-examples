package com.seda.codeeval.easy.nmodm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			String[] nmArray = line.split(",");
			int n = Integer.parseInt(nmArray[0]);
			int m = Integer.parseInt(nmArray[1]);
			System.out.println(n % m);
		}
		buffer.close();
	}
}
