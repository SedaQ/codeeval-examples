package com.seda.codeeval.easy.wordtodigit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			String[] wordArray = line.split(";");
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < wordArray.length; i++) {
				switch (wordArray[i].toLowerCase()) {
				case "zero":
					sb.append(0);
					break;
				case "one":
					sb.append(1);
					break;
				case "two":
					sb.append(2);
					break;
				case "three":
					sb.append(3);
					break;
				case "four":
					sb.append(4);
					break;
				case "five":
					sb.append(5);
					break;
				case "six":
					sb.append(6);
					break;
				case "seven":
					sb.append(7);
					break;
				case "eight":
					sb.append(8);
					break;
				case "nine":
					sb.append(9);
					break;
				default:
					sb.append("");
				}
			}
			System.out.println(sb.toString());
		}
		buffer.close();
	}
}
