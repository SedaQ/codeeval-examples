package com.seda.codeeval.easy.sumdigits;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			int sum = 0;
			char[] arr = line.trim().toCharArray();
			for (int i = 0; i < arr.length; i++) {
				sum += Character.getNumericValue(arr[i]);
			}
			System.out.println(sum);
		}
	}
}
