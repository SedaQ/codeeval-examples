package com.seda.codeeval.easy.majorelement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] numbers = line.split(",");
			Arrays.sort(numbers);
			if (numbers.length > 0) {
				System.out.println(numbers[0]);
			}
		}
	}
}
