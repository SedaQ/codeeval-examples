package com.seda.codeeval.easy.fibonacciseries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(fibonacci(Integer.parseInt(line.trim())));
		}
		br.close();
	}

	public static int fibonacci(int number) {
		if (number == 0)
			return 0;
		else if (number == 1) return 1;

		int result = 2;
		int firstN = 1;
		int secondN = 0;
		for (int i = 2; i <= number; i++) {
			result = firstN + secondN;
			secondN = firstN;
			firstN = result;
		}
		return result;
	}
}
