package com.seda.codeeval.easy.swapelements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			char[] arr = line.toCharArray();
			for (int i = 0; i < arr.length; i++) {
				if (Character.isUpperCase(arr[i])) {
					arr[i] = Character.toLowerCase(arr[i]);
				} else if (Character.isLowerCase(arr[i])) {
					arr[i] = Character.toUpperCase(arr[i]);
				}
			}
			System.out.println(new String(arr));
		}
	}
}
