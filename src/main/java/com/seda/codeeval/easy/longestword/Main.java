package com.seda.codeeval.easy.longestword;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] words = line.trim().split(" ");
			int maxLength = 0;
			String longestWord = "";
			for (int i = 0; i < words.length; i++) {
				if (words[i].length() > maxLength) {
					maxLength = words[i].length();
					longestWord = words[i];
				}
			}
			System.out.println(longestWord);
		}
		bf.close();
	}
}
