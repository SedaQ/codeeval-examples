package com.seda.codeeval.easy.realfake;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestCaseExample {

	public static void main(String[] args) throws IOException {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\realfake\\test.txt");
		BufferedReader bf = new BufferedReader(new FileReader(f.getAbsolutePath()));
		String line = "";
		while ((line = bf.readLine()) != null) {
			int sum = 0;
			int temp = 1;
			char[] arr = line.toCharArray();
			for (int i = 0; i < arr.length; i++) {
				if ((Character.isDigit(arr[i]))) {
					if (temp % 2 != 0) {
						sum += Integer.parseInt(String.valueOf(arr[i])) * 2;
						System.out.print(arr[i]);
					}
					temp++;
				}
			}
			System.out.println(isFake(sum) ? " Fake" : " Real");
		}
		bf.close();
	}

	public static boolean isFake(int n) {
		return n % 10 != 0;
	}
}
