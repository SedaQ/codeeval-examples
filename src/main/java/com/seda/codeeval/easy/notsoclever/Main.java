package com.seda.codeeval.easy.notsoclever;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			System.out.println(line);
			String[] numbers = line.split("\\|");

			String[] arr = numbers[0].trim().split(" ");
			int arrLength = arr.length;
			int[] array = new int[arrLength];
			for (int i = 0; i < arrLength; i++) {
				array[i] = Integer.parseInt(arr[i]);
			}
			int[] arra = stupidSort(array, Integer.parseInt(numbers[1].trim()));
			for (int i = 0; i < arra.length; i++) {
				System.out.print(i < arra.length ? arra[i] + " " : arra[i]);
			}
			System.out.println();
			System.out.println(Arrays.toString(stupidSort(array, Integer.parseInt(numbers[1].trim()))));
		}
	}

	public static int[] stupidSort(int[] arr, int n) {
		if (n == 0) {
			return arr;
		}
		int number = 0;
		for (int i = number; i < arr.length; i++) {
			if ((arr.length > number + 1) && (arr[number] > arr[number + 1])) {
				int temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
				if (++number == n) {
					break;
				}
			}
		}
		return arr;
	}
}
