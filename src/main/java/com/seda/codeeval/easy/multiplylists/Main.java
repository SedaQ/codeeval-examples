package com.seda.codeeval.easy.multiplylists;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\multiplylists\\test.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String line = "";
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				String[] lineArr = line.split("|");
				int[] firstList = Arrays.asList(lineArr[0].trim().split(" ")).stream().mapToInt(Integer::parseInt)
						.toArray();
				int[] secondList = Arrays.asList(lineArr[1].trim().split(" ")).stream().mapToInt(Integer::parseInt)
						.toArray();

				for (int i = 0; i < firstList.length; i++) {
					System.out.println(firstList[i]);
					// System.out.print(firstList[i] * secondList[i] + " ");
				}
				System.out.println();
			}
		} catch (IOException io) {
		}
	}
}
