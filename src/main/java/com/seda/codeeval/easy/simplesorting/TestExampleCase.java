package com.seda.codeeval.easy.simplesorting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class TestExampleCase {

	public static void main(String[] args) throws IOException {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\simplesorting\\test.txt");
		BufferedReader bf = new BufferedReader(new FileReader(f.getAbsolutePath()));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] arr = line.trim().split(" ");
			Arrays.sort(arr, (a1, a2) -> Double.valueOf(a1).compareTo(Double.valueOf(a2)));
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
		bf.close();
	}
}
