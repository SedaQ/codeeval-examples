package com.seda.codeeval.easy.simplesorting;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = br.readLine()) != null) {
			String[] arr = line.trim().split(" ");
			Arrays.sort(arr, (a1, a2) -> Double.valueOf(a1).compareTo(Double.valueOf(a2)));
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
		br.close();
	}
}
