package com.seda.codeeval.easy.percentageratio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		int lowerCase = 0;
		int upperCase = 0;
		int lineLength = 0;
		char[] c = null;
		while ((line = buffer.readLine()) != null) {
			c = line.trim().toCharArray();
			lowerCase = 0;
			upperCase = 0;
			lineLength = c.length;

			for (int i = 0; i < lineLength; i++) {
				if (Character.isUpperCase(c[i])) {
					upperCase++;
				} else if (Character.isLowerCase(c[i])) {
					lowerCase++;
				}
			}
			System.out.println("lowercase: " + String.format("%.2f", ((double) lowerCase / lineLength * 100))
					+ " uppercase: " + String.format("%.2f", (double) upperCase / lineLength * 100));
		}
		buffer.close();
	}

}