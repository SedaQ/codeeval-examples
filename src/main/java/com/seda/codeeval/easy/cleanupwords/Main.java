package com.seda.codeeval.easy.cleanupwords;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		String line = "";
		try (BufferedReader bf = new BufferedReader(new FileReader(args[0]))) {
			while ((line = bf.readLine()) != null) {
				char[] chars = line.toCharArray();
				for (int i = 0; i < chars.length; i++) {
					if (Character.isLetter(chars[i])) {
						sb.append(Character.toLowerCase(chars[i]));
					} else if (i > 0 && Character.isLetter(chars[i - 1])) {
						sb.append(" ");
					}
				}
				sb.append(System.lineSeparator());
			}
			System.out.println(sb.toString());
		} catch (IOException io) {
		}
	}
}
