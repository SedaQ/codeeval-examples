package com.seda.codeeval.easy.datarecovery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\datarecovery\\test.txt");
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				String[] delArray = line.split(";");
				String[] joke = delArray[0].trim().split(" ");
				String[] jokeWordsRanking = delArray[1].trim().split(" ");
				int[] jokeRanking = Arrays.asList(jokeWordsRanking).stream().mapToInt(Integer::parseInt).toArray();
				Map<Integer, String> map = new TreeMap<>();
				for (int i = 1; i < jokeRanking.length; i++) {
					System.out.print(jokeRanking[i] + " ");
					System.out.print(joke[jokeRanking[i - 1] - 1] + " ");
					map.put(jokeRanking[i], joke[jokeRanking[i] - 1]);
				}
				// map.forEach((k, v) -> System.out.println("Key: " + k +
				// "Value: " + v));
				System.out.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public static String getWord(String[] joke, int[] jokeRanking, int rank)
	// {
	// for (int i = 0; i < jokeRanking.length; i++) {
	// System.out.print(rank + " ");
	// if (jokeRanking[i] == rank && (rank + 1 < jokeRanking.length)) {
	// System.out.print(new StringBuilder(joke[i]).append(" ").toString());
	//
	// ++rank;
	// getWord(joke, jokeRanking, rank);
	// }
	// }
	// return "";
	// }
}