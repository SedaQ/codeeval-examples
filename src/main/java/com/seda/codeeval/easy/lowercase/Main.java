package com.seda.codeeval.easy.lowercase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			System.out.println(line.toLowerCase());
		}
	}
}
