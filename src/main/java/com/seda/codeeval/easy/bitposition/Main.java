package com.seda.codeeval.easy.bitposition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File f = new File(args[0]);
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				String[] bits = line.split(",");
				String binary = Integer.toBinaryString(Integer.parseInt(bits[0]));
				if (binary.charAt(Integer.parseInt(bits[1]) - 1) == (binary.charAt(Integer.parseInt(bits[2]) - 1))) {
					System.out.println("true");
				} else {
					System.out.println("false");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}