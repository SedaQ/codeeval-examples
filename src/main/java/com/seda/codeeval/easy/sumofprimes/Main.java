package com.seda.codeeval.easy.sumofprimes;

public class Main {

	public static void main(String[] args) {
		int count = 0;
		int sum = 0;
		int i = 2;
		while (count < 1000) {
			if (isPrime(i)) {
				sum += i;
				count++;
			}
			i++;
		}
		System.out.println(sum);

	}

	public static boolean isPrime(int n) {
		int root = (int) Math.sqrt(n);
		for (int i = 2; i <= root; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

}
