package com.seda.codeeval.easy.findawriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			String[] splitter = line.split("\\|");
			StringBuilder testSequence = new StringBuilder(splitter[0].trim());
			String[] indexes = splitter[1].trim().split(" ");
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indexes.length; i++) {
				sb.append(testSequence.charAt(Integer.parseInt(indexes[i]) - 1));
			}
			System.out.println(sb.toString());
		}
		buffer.close();
	}
}