package com.seda.codeeval.easy.setintersection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

// podiat se pozdeji
public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = br.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			String[] temp = line.split("\\;");
			String[] firstSet = temp[0].split(",");
			String[] secondSet = temp[1].split(",");
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < firstSet.length; i++) {
				for (int j = 0; j < secondSet.length; j++) {
					if (firstSet[i].equals(secondSet[j])) {
						sb.append(firstSet[i]).append(",");
					}
				}
			}
			if (sb.charAt(sb.length() - 1) == ',') {
				sb.deleteCharAt(sb.length() - 1);
			}
			System.out.println(sb.toString());
		}
		br.close();
	}
}
