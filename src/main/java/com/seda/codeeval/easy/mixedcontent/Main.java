package com.seda.codeeval.easy.mixedcontent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		File f = new File(args[0]);
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				String[] lineArray = line.split(",");
				List<String> words = new ArrayList<>();
				List<String> numbers = new ArrayList<>();

				for (int i = 0; i < lineArray.length; i++) {
					if (lineArray[i].matches(".*\\d+.*")) {
						numbers.add(lineArray[i]);
					} else {
						words.add(lineArray[i]);
					}
				}
				String w = words.stream().collect(Collectors.joining(",")).trim();
				String n = numbers.stream().collect(Collectors.joining(",")).trim();
				if (!w.equals("") && !n.equals("")) {
					System.out.println(w + "|" + n);
				} else if (w.equals("")) {
					System.out.println(n);
				} else {
					System.out.println(w);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
