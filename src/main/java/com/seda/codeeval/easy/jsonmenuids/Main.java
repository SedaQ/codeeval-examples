package com.seda.codeeval.easy.jsonmenuids;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader bf = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = bf.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			int sum = 0;
			Pattern p = Pattern.compile("\\\"id\\\": \\d+, \\\"label\\\": \\\"Label (\\d+)\\\"");
			Matcher m = p.matcher(line);
			while (m.find()) {
				sum += Integer.parseInt(m.group(1));
			}
			System.out.println(sum);

		}
		bf.close();
	}
}
