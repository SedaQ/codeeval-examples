package com.seda.codeeval.easy.hiddendigits;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File f = new File(args[0]);
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				boolean hasDigit = false;
				char[] charArr = line.toCharArray();
				for (char c : charArr) {
					if (Character.isDigit(c)) {
						System.out.print(c);
						hasDigit = true;
						continue;
					}
					switch (c) {
					case 'a':
						System.out.print(0);
						hasDigit = true;
						continue;
					case 'b':
						System.out.print(1);
						hasDigit = true;
						continue;
					case 'c':
						System.out.print(2);
						hasDigit = true;
						continue;
					case 'd':
						System.out.print(3);
						hasDigit = true;
						continue;
					case 'e':
						System.out.print(4);
						hasDigit = true;
						continue;
					case 'f':
						System.out.print(5);
						hasDigit = true;
						continue;
					case 'g':
						System.out.print(6);
						hasDigit = true;
						continue;
					case 'h':
						System.out.print(7);
						hasDigit = true;
						continue;
					case 'i':
						System.out.print(8);
						hasDigit = true;
						continue;
					case 'j':
						System.out.print(9);
						hasDigit = true;
						continue;
					default:
						continue;
					}
				}
				if (hasDigit == true)
					System.out.println();
				else
					System.out.println("NONE");
			}
		} catch (IOException io) {

		}
	}
}
