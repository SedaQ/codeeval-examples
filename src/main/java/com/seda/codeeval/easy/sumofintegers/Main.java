package com.seda.codeeval.easy.sumofintegers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line = "";
		int sum = 0;
		while ((line = br.readLine()) != null) {
			sum += Integer.parseInt(line.trim());
		}
		System.out.println(sum);
	}
}
