package com.seda.codeeval.easy.primepalindrome;

public class Main {

	public static void main(String[] args) {
		int largestPrimePalindrome = 2;

		for (int i = 0; i < 1000; i++) {
			if (isPrime(i) && isPalindrome(String.valueOf(i))) {
				largestPrimePalindrome = i;
			}
		}
		System.out.println(largestPrimePalindrome);
	}

	public static boolean isPalindrome(String sb) {
		return sb.equals(new StringBuilder(sb).reverse().toString());
	}

	public static boolean isPrime(int n) {
		int root = (int) Math.sqrt(n);

		for (int i = 2; i <= root; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
