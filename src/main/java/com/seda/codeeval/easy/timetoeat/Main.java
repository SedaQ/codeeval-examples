package com.seda.codeeval.easy.timetoeat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) continue;
			String[] datesArr = line.split(" ");
			Arrays.sort(datesArr, Collections.reverseOrder());
			for (int i = 0; i < datesArr.length; i++) {
				if (i + 1 == datesArr.length) {
					System.out.println(datesArr[i]);
				} else {
					System.out.print(datesArr[i] + " ");
				}
			}

		}
		buffer.close();
	}
}
