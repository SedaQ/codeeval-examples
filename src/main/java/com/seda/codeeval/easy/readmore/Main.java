package com.seda.codeeval.easy.readmore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File f = new File(args[0]);
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0)
					continue;
				else if (line.length() > 55) {
					StringBuilder sb = new StringBuilder();
					char[] c = line.toCharArray();
					for (int i = 0; i < 40; i++) {
						sb.append(c[i]);
					}
					String temp = sb.toString().trim();
					System.out.println(new StringBuilder(temp).append("... <Read More>").toString());
				} else {
					System.out.println(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
