package com.seda.codeeval.easy.splitnumber;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * You are given a number N and a pattern. The pattern consists of lowercase
 * latin letters and one operation "+" or "-". The challenge is to split the
 * number and evaluate it according to this pattern e.g. 1232 ab+cd -> a:1, b:2,
 * c:3, d:2 -> 12+32 -> 44
 * 
 * @author Seda
 *
 */
public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] nl = line.split(" ");
			int temp1 = nl[1].indexOf("+");
			int index = temp1 != -1 ? temp1 : nl[1].indexOf("-");
			StringBuilder sb = new StringBuilder();
			char[] arr = nl[0].toCharArray();
			for (int i = 0; i < index; i++) {
				sb.append(arr[i]);
			}
			int firstNumber = Integer.parseInt(sb.toString());
			sb = new StringBuilder();
			for (int i = index; i < nl[0].length(); i++) {
				sb.append(arr[i]);
			}
			int result = temp1 != -1 ? firstNumber + Integer.parseInt(sb.toString())
					: firstNumber - Integer.parseInt(sb.toString());
			System.out.println(result);
		}
	}
}
