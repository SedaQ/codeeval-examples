package com.seda.codeeval.easy.splitnumber;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestCaseExample {

	public static void main(String[] args) throws IOException {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\splitnumber\\test.txt");
		BufferedReader bf = new BufferedReader(new FileReader(f.getAbsolutePath()));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] nl = line.split(" ");
			int temp1 = nl[1].indexOf("+");
			int index = temp1 != -1 ? temp1 : nl[1].indexOf("-");
			StringBuilder sb = new StringBuilder();
			char[] arr = nl[0].toCharArray();
			for (int i = 0; i < index; i++) {
				sb.append(arr[i]);
			}
			int firstNumber = Integer.parseInt(sb.toString());
			sb = new StringBuilder();
			for (int i = index; i < nl[0].length(); i++) {
				sb.append(arr[i]);
			}
			int result = temp1 != -1 ? firstNumber + Integer.parseInt(sb.toString())
					: firstNumber - Integer.parseInt(sb.toString());
			System.out.println(result);
		}
	}
}