package com.seda.codeeval.easy.reversewords;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(args[0]));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] words = line.split(" ");
			for (int i = words.length - 1; i > -1; i--) {
				if (i != 0) {
					System.out.print(words[i] + " ");
				} else {
					System.out.print(words[i]);
				}
			}
			System.out.println();
		}
		bf.close();
	}
}
