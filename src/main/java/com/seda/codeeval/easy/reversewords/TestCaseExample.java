package com.seda.codeeval.easy.reversewords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestCaseExample {

	public static void main(String[] args) throws IOException {
		File f = new File(
				"C:\\EclipseWorkspace\\CodeEvalExamples\\src\\main\\java\\com\\seda\\codeeval\\easy\\reversewords\\test.txt");
		BufferedReader bf = new BufferedReader(new FileReader(f.getAbsolutePath()));
		String line = "";
		while ((line = bf.readLine()) != null) {
			String[] words = line.split(" ");
			for (int i = words.length - 1; i > -1; i--) {
				if (i != 0) {
					System.out.print(words[i] + " ");
				} else {
					System.out.print(words[i]);
				}
			}
			System.out.println();
		}
		bf.close();
	}
}