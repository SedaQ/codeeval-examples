package com.seda.codeeval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Main template for set of examples
 * 
 * @author Seda
 *
 */
public class Main {

	public static void main(String[] args) {
		File f = new File(args[0]);
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
