package com.seda.codeeval.moderate.numberofones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			String binary = Integer.toBinaryString(Integer.parseInt(line.trim()));
			char[] b = binary.toCharArray();
			int count = 0;
			for (int i = 0; i < b.length; i++) {
				if (b[i] == '1') {
					count++;
				}
			}
			System.out.println(count);
		}
		br.close();
	}
}
