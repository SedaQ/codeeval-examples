package com.seda.codeeval.moderate.removecharacter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			String[] s1 = line.split(",");
			String sen = s1[0].trim();
			String charRemove = s1[1].trim();
			StringBuilder sb = new StringBuilder();

			for (char c : sen.toCharArray()) {
				if (!charRemove.contains(String.valueOf(c))) {
					sb.append(c);
				}
			}

			System.out.println(sb.toString());
		}
		br.close();
	}
}
