package com.seda.codeeval.moderate.pangrams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader bf = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = bf.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			line = line.trim().toLowerCase();
			char[] c = line.toCharArray();

			String alphabetic = "abcdefghijklmnopqrstuvwxyz";
			for (int i = 0; i < c.length; i++) {
				if (Character.isAlphabetic(c[i])) {
					alphabetic = alphabetic.replace(String.valueOf(c[i]), "");
				}
			}
			if (alphabetic.trim().isEmpty()) {
				System.out.println("NULL");
			} else {
				System.out.println(alphabetic);
			}
		}
		bf.close();
	}
}
