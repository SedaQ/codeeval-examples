package com.seda.codeeval.moderate.longestlines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		int temp = Integer.parseInt(br.readLine().trim());
		Queue<Lines> pQ = new PriorityQueue<>((o1, o2) -> o2.getLength() - o1.getLength());
		while ((line = br.readLine()) != null) {
			pQ.add(new Lines(line, line.length()));
		}
		for (int i = 0; i < temp; i++) {
			System.out.println(pQ.poll().getValue());
		}
		br.close();
	}

}

class Lines {
	private String value;
	private int length;

	Lines() {
		this.value = "";
		length = 0;
	}

	Lines(String value, int length) {
		this.value = value;
		this.length = length;
	}

	public String getValue() {
		return this.value;
	}

	public int getLength() {
		return this.length;
	}
}