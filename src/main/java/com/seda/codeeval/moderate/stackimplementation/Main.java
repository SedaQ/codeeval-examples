package com.seda.codeeval.moderate.stackimplementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			String[] l = line.trim().split(" ");
			MyOwnStack<Integer> stack = new MyOwnStack<>();
			for (int i = 0; i < l.length; i++) {
				stack.push(Integer.parseInt(l[i]));
			}
			int length = stack.size();
			for (int i = 0; i < length; i++) {
				if (i % 2 == 0) {
					System.out.print(stack.pop() + " ");
				} else {
					stack.pop();
				}
			}
			System.out.println();
		}
		br.close();
	}
}

class MyOwnStack<T> {
	private int total;
	private Node<T> item;
	private Node<T> topItem;

	public MyOwnStack(T item) {
		this.item = new Node<T>(item);
	}

	public MyOwnStack() {
	}

	public void push(T item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot push null item to Stack");
		}
		if (this.item == null) {
			this.item = new Node<T>(item);
			this.topItem = this.item;
			this.total++;
		} else {
			Node<T> temp = new Node<T>(item);
			this.topItem = temp;
			this.topItem.setNext(this.item);
			this.item = this.topItem;
			this.total++;
		}
	}

	public T pop() {
		if (this.item == null) {
			throw new NullPointerException("You cannot pop from Stack which is empty");
		} else {
			this.total--;
			Node<T> temp = this.item;
			this.topItem = this.topItem.getNext();
			this.item = this.topItem;
			return temp.getData();
		}
	}

	public int size() {
		return this.total;
	}

}

class Node<T> {
	private Node<T> next;
	private T data;

	public Node(T data) {
		this.data = data;
	}

	public T getData() {
		return this.data;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

	public Node<T> getNext() {
		return this.next;
	}
}
