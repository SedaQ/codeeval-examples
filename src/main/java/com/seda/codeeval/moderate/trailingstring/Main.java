package com.seda.codeeval.moderate.trailingstring;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			if (line.trim().length() == 0) continue;
			String[] temp = line.trim().split(",");
			String testString = temp[0].trim();
			String pattern = temp[1].trim();
			Pattern p = Pattern.compile(".*" + (pattern));
			Matcher m = p.matcher(testString);
			if (m.find()) {
				System.out.println("1");
			} else {
				System.out.println("0");
			}
		}
		br.close();
	}
}
