import sys
from struct import pack

with open(sys.argv[1], 'r') as test_cases:
    if pack('@h', 1) == pack('<h', 1):
        print ("LittleEndian")
    else:
        print ("BigEndian")
