package com.seda.codeeval.moderate.endianness;

import java.nio.ByteOrder;

/**
 * https://en.wikipedia.org/wiki/Endianness
 *
 */
public class Main {

	public static void main(String[] args) {
		if (ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN)) {
			System.out.println("BigEndian");
		} else {
			System.out.println("LittleEndian");
		}

	}
}
