package com.seda.codeeval.moderate.decimaltobinary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		File f = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		while ((line = br.readLine()) != null) {
			System.out.println(Integer.toBinaryString(Integer.parseInt(line.trim())));
		}
		br.close();
	}
}
