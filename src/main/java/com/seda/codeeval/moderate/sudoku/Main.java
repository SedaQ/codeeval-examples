package com.seda.codeeval.moderate.sudoku;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Main {

	// store unique values, good for testing if line contains digits from 1 to 9
	// or 1 to 4
	private static Set<Integer> set = new HashSet<Integer>();
	private static int sudokuSize = 0;

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = buffer.readLine()) != null) {
			String[] lineArray = line.trim().split(";");
			String[] gridValues = lineArray[1].split(",");
			sudokuSize = Integer.parseInt(lineArray[0]);
			int[][] aline = new int[sudokuSize][sudokuSize];
			int arrIndex = 0;
			for (int i = 0; i < sudokuSize; i++) {
				for (int j = 0; j < sudokuSize; j++) {
					aline[i][j] = Integer.parseInt(gridValues[arrIndex++]);
				}
			}

			boolean result = testSudokuSubsquares(aline, 0, 0);
			if (!result) {
				System.out.println("False");
			} else {
				boolean res = testSudokuRowsAndColumns(aline);
				if (!res) {
					System.out.println("False");
				} else {
					System.out.println("True");
				}
			}
		}
		buffer.close();
	}

	private static boolean testSudokuRowsAndColumns(int[][] aline) {
		set.clear();
		for (int i = 0; i < sudokuSize; i++) {
			for (int j = 0; j < sudokuSize; j++) {
				set.add(aline[i][j]);
			}
			if (set.size() != sudokuSize) {
				return false;
			}
			set.clear();
		}

		for (int i = 0; i < sudokuSize; i++) {
			for (int j = 0; j < sudokuSize; j++) {
				set.add(aline[j][i]);
			}
			if (set.size() != sudokuSize) {
				return false;
			}
			set.clear();
		}

		return true;
	}

	private static boolean testSudokuSubsquares(int[][] aline, int x, int y) {
		set.clear();
		int subsquareSize = (int) Math.sqrt(sudokuSize);
		for (int i = 0; i < subsquareSize; i++) {
			for (int j = 0; j < subsquareSize; j++) {
				// x,y determines which cordinates it takes from sudoku grid
				set.add(aline[i + y][j + x]);
			}
		}
		// if subsquare contains all unique numbers
		if (set.size() != sudokuSize) {
			return false;
		}

		if ((x == sudokuSize - subsquareSize) && (y == sudokuSize - subsquareSize)) {
			return true;
		}

		// start subquare from x,y coordinates from sudoku grid
		if (x != (sudokuSize - subsquareSize)) {
			testSudokuSubsquares(aline, x + subsquareSize, y);
		} else {
			if (x == (sudokuSize - subsquareSize)) {
				x = 0;
				testSudokuSubsquares(aline, x, y + subsquareSize);
			} else {
				testSudokuSubsquares(aline, x + subsquareSize, y);
			}

		}

		return true;
	}

}
